import asyncio
import sdp_transform

class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received SDP offer from', addr)

        # Parse the SDP offer
        offer = sdp_transform.parse(message)

        # Update the ports in the answer
        offer['media'][0]['port'] = 34543
        offer['media'][1]['port'] = 34543

        print(offer['media'][0]['port'], offer['media'][1]['port'])
        print('Sending SDP answer...')

        # Generate the SDP answer
        answer = sdp_transform.write(offer)

        # Send the answer back to the client
        self.transport.sendto(answer.encode(), addr)

async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # Create the UDP server
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(), local_addr=('127.0.0.1', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

asyncio.run(main())